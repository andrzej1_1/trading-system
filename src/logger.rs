use chrono::Local;

/// Logs message to stdout, pefixed with current time.
pub fn log(msg: String) {
  let now = Local::now();
  println!("[{}] {}", now.to_string(), msg);
}
