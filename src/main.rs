use anyhow::{Context, Result};
use tokio::runtime::Builder;

mod client_handler;
mod logger;
mod parser;
mod server;

fn main() -> Result<()> {
  // Build asynchronous runtime that use current thread.
  let runtime = Builder::new_current_thread()
    .enable_all()
    .build()
    .context("Unable to build async runtime.")?;

  // Create server and run it asynchronously.
  let server = server::Server::new();
  runtime.block_on(async {
    server
      .run("0.0.0.0:8888")
      .await
      .context("Error occured while running server.")
  })?;

  Ok(())
}
