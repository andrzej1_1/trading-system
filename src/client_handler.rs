use std::sync::Arc;

use thiserror::Error as ThisError;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;
use tokio::sync::broadcast;

use trading_system::core::engine::{Engine, TradeResult};
use trading_system::core::types::ClientId;

use crate::parser::CliCommand;
use crate::{logger, parser};

/// Client handler error.
#[derive(ThisError, Debug)]
pub enum Error {
  /// Unable to obtain remote peer address.
  #[error("Unable to retrieve peer address.")]
  MissingPeerAddr,
  /// Data is not parsable as utf-8.
  #[error("Non utf-8 data received.")]
  NonUtf8Data,
  /// Internal communication issue.
  #[error("Unable to send/recv broadcast message.")]
  InternalIO,
  /// Any connection issue.
  #[error(transparent)]
  IO(#[from] std::io::Error),
}

/// Handler for client connection.
pub struct ClientHandler {
  /// Client connection.
  stream: TcpStream,
  /// Reference to trading Engine.
  engine: Arc<Engine>,
  /// Sender for broadcasting channel.
  broadcast_tx: broadcast::Sender<String>,
  /// Receiver for broadcasting channel.
  broadcast_rx: broadcast::Receiver<String>,
  /// Client ID.
  client_id: ClientId,
}

impl ClientHandler {
  /// Constructs new ClientHandler.
  pub fn new(
    stream: TcpStream,
    engine: Arc<Engine>,
    broadcast_tx: broadcast::Sender<String>,
    broadcast_rx: broadcast::Receiver<String>,
  ) -> Result<Self, Error> {
    // Port is considered as client ID.
    let client_id = stream
      .peer_addr()
      .map_err(|_e| Error::MissingPeerAddr)?
      .port()
      .into();
    logger::log(format!("Client {} connected.", client_id));

    Ok(Self {
      stream,
      engine,
      broadcast_tx,
      broadcast_rx,
      client_id,
    })
  }

  /// Handles incoming data. Works until client disconnect or serious error
  /// occurs. If user passes invalid command then loop is continued.
  pub async fn run(&mut self) -> Result<(), Error> {
    loop {
      // Prepare 128 bytes buffer; that size is enough for client and broadcast data.
      let mut buffer = [0; 128];

      // Wait for broadcast message or user command.
      tokio::select! {
        val = self.broadcast_rx.recv() => {
          // Send broadcasted message to the client.
          match val {
            Ok(msg) => { self.stream.write(msg.as_bytes()).await?; },
            Err(_e) => {},
          }
        }
        val = self.stream.read(&mut buffer) => {
          match val {
            Ok(n) => {
              if n == 0 {
                // Connection was closed.
                logger::log(format!("Client {} disconnected.", self.client_id));
                break;
              }
              // Parse data and handle it as command.
              match std::str::from_utf8(&buffer[0..n]) {
                Err(_e) => {
                  return Err(Error::NonUtf8Data);
                }
                Ok(line) => match parser::CliCommand::new(line.trim_end()) {
                  Err(_e) => {
                    self.reply("Invalid command received.\n").await?;
                  }
                  Ok(cmd) => { self.handle_command(cmd).await?; }
                },
              }
            }
            Err(e) => {
              return Err(e.into());
            }
          }
        }
      }
    }

    Ok(())
  }

  /// Sends message to the connected client.
  /// In most cases you should add '\n' at the end of message.
  async fn reply(&mut self, msg: &str) -> Result<(), Error> {
    self.stream.write(msg.as_bytes()).await?;
    Ok(())
  }

  /// Sends ACK message to the connected client.
  async fn reply_with_ack(&mut self, product: &str) -> Result<(), Error> {
    let msg = format!("ACK:{}\n", product);
    self.reply(&msg).await?;
    Ok(())
  }

  /// Sends TRADE message among all connected clients.
  async fn broadcast_trade(&mut self, product: &str) -> Result<(), Error> {
    let msg = format!("TRADE:{}\n", product);
    self
      .broadcast_tx
      .send(msg)
      .map_err(|_e| Error::InternalIO)?;
    Ok(())
  }

  /// Processes command with engine. If it was processed successfully
  /// then replies with ACK.
  ///
  /// If trade occurs (here or by other client) then every user is notified
  /// with TRADE message.
  async fn handle_command(&mut self, cmd: CliCommand) -> Result<(), Error> {
    // Convert user command into Order.
    let order_type = cmd.order_type().to_string().to_uppercase();
    let prod_name = cmd.product_type().to_string().to_uppercase();
    logger::log(format!(
      "New {} order for {} (client: {}).",
      order_type, prod_name, self.client_id
    ));
    let order = cmd.transform_to_order(self.client_id);

    // Feed engine with new order.
    match self.engine.process(order) {
      Ok(trade_result) => {
        // Valid order - reply with ACK.
        self.reply_with_ack(&prod_name).await?;
        if let TradeResult::Transaction(cl_a, cl_b) = trade_result {
          // Trade was executed: broadcast message about it.
          logger::log(format!(
            "Trading {} between {} and {}.",
            prod_name, cl_a, cl_b
          ));
          self.broadcast_trade(&prod_name).await?;
        }
      }
      Err(_e) => {
        self.reply("Unable to process order.\n").await?;
      }
    };

    Ok(())
  }
}
