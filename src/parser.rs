use thiserror::Error as ThisError;

use trading_system::core::types::{ClientId, Order, OrderType, ProductBox, ProductType};

/// Parser error.
#[derive(ThisError, Debug)]
pub enum Error {
  /// Order type was not provided.
  #[error("Misssing order type")]
  MissingOrder,
  /// Order type is not recognized.
  #[error("Invalid order type.")]
  InvalidOrder,
  /// Product type was not provided.
  #[error("Misssing product type")]
  MissingProduct,
  /// Product type is not recognized.
  #[error("Invalid product type.")]
  InvalidProduct,
}

/// Command received from client.
#[derive(Debug)]
pub struct CliCommand {
  /// Order type.
  order_type: OrderType,
  /// Product type.
  product_type: ProductType,
}

impl CliCommand {
  /// Converts text command into CliCommand structure.
  /// Parsing is case-insensitive.
  ///
  /// Example valid inputs:
  /// - "BUY:ONION"
  /// - "SELL:APPLE"
  pub fn new(cmd: &str) -> Result<Self, Error> {
    // Split command into two parts, then parse each one.
    let parts: Vec<String> = cmd.split(':').map(str::to_lowercase).collect();
    let order_type = match parts.get(0).ok_or(Error::MissingOrder)?.as_ref() {
      "buy" => OrderType::Buy,
      "sell" => OrderType::Sell,
      _ => {
        return Err(Error::InvalidOrder);
      }
    };
    let product_type = match parts.get(1).ok_or(Error::MissingProduct)?.as_ref() {
      "apple" => ProductType::Apple,
      "pear" => ProductType::Pear,
      "tomato" => ProductType::Tomato,
      "potato" => ProductType::Potato,
      "onion" => ProductType::Onion,
      _ => {
        return Err(Error::InvalidProduct);
      }
    };

    Ok(Self {
      order_type,
      product_type,
    })
  }

  /// Gets order type.
  pub fn order_type(&self) -> &OrderType {
    &self.order_type
  }

  /// Gets product type.
  pub fn product_type(&self) -> &ProductType {
    &self.product_type
  }

  /// Transforms current command into order.
  pub fn transform_to_order(self, client_id: ClientId) -> Order {
    let order_type = self.order_type;
    let product_box = ProductBox::new(self.product_type);
    Order::new(client_id, order_type, product_box)
  }
}
