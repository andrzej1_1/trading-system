use std::fmt;

use strum_macros::EnumIter;

/// Different types of products.
#[derive(Debug, EnumIter, PartialEq, Eq, Hash, Clone)]
pub enum ProductType {
  Apple,
  Pear,
  Tomato,
  Potato,
  Onion,
}

impl fmt::Display for ProductType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{:?}", self)
  }
}

/// Box containing product.
/// There is no specific amount or measure, just one box with product inside.
#[derive(Debug)]
pub struct ProductBox(ProductType);

impl ProductBox {
  /// Creates new box.
  pub fn new(product_type: ProductType) -> Self {
    ProductBox(product_type)
  }

  /// Gets product type.
  pub fn product_type(&self) -> &ProductType {
    &self.0
  }
}

/// Client ID.
pub type ClientId = u32;

/// Type of order.
#[derive(Debug)]
pub enum OrderType {
  /// Buy request.
  Buy,
  /// Sell request.
  Sell,
}

impl fmt::Display for OrderType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{:?}", self)
  }
}

/// Order tied to specific user.
#[derive(Debug)]
pub struct Order {
  /// Client that placed order.
  client_id: ClientId,
  /// Type of order.
  order_type: OrderType,
  /// What was ordered.
  product_box: ProductBox,
}

impl Order {
  /// Creates new order.
  pub fn new(client_id: ClientId, order_type: OrderType, product_box: ProductBox) -> Self {
    Self {
      client_id,
      order_type,
      product_box,
    }
  }

  /// Gets client ID.
  pub fn client_id(&self) -> &ClientId {
    &self.client_id
  }

  /// Gets order type.
  pub fn order_type(&self) -> &OrderType {
    &self.order_type
  }

  /// Gets box.
  pub fn product_box(&self) -> &ProductBox {
    &self.product_box
  }
}
