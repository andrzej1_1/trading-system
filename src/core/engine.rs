use std::collections::{HashMap, VecDeque};
use std::sync::{Mutex, PoisonError};

use strum::IntoEnumIterator;
use thiserror::Error as ThisError;

use crate::core::types::*;

/// Engine error.
#[derive(ThisError, Debug)]
pub enum Error {
  /// Storage for product orders cannot be found.
  #[error("Orders storage not found.")]
  StorageNotFound,
  /// Storage (mutex) with orders cannot be locked.
  #[error("Unable to acquire lock.")]
  StorageLocked,
}

impl<T> From<PoisonError<T>> for Error {
  fn from(_err: PoisonError<T>) -> Self {
    Self::StorageLocked
  }
}

/// Result of user placing new order, it can be either
/// matched pair (trade is made) or no match.
#[derive(Debug)]
pub enum TradeResult {
  /// No matching pair found.
  NoMatch,
  /// Trade was executed between clients.
  Transaction(ClientId, ClientId),
}

/// Structure that holds two queues with client IDs:
/// one queue is used to store 'buy' requests, the other
/// one to store 'sell' requests.
///
/// This queue should be used for single product type to
/// easily find matching pairs.
#[derive(Debug, Default)]
pub struct BuySellQueue {
  /// List of clients that want to buy thing.
  buy_orders: VecDeque<ClientId>,
  /// List of clients that want to sell thing.
  sell_orders: VecDeque<ClientId>,
}

impl BuySellQueue {
  /// Determines main and opposite queue for given order type.
  /// Useful for find matching pairs among buy/sell orders.
  fn get_queues(
    &mut self,
    order_type: &OrderType,
  ) -> (&mut VecDeque<ClientId>, &mut VecDeque<ClientId>) {
    match order_type {
      OrderType::Buy => (&mut self.buy_orders, &mut self.sell_orders),
      OrderType::Sell => (&mut self.sell_orders, &mut self.buy_orders),
    }
  }
}

/// Engine for processing orders and finding matching buy-sell pair.
#[derive(Default, Debug)]
pub struct Engine {
  /// Storage for orders.
  ///
  /// We use following HashMap:
  /// - key: product type (enum)
  /// - value: corresponding buy queue and sell queue (2x VecDeque internally)
  /// This design allows to work on different produts at the same time,
  /// without locking the entire map.
  orders: HashMap<ProductType, Mutex<BuySellQueue>>,
}

impl Engine {
  /// Constructs new Engine.
  pub fn new() -> Self {
    // Create hashmap containing buy/sell registries, prefilled
    // with mutexed queues on every possible product type.
    let mut orders = HashMap::new();
    for product_type in ProductType::iter() {
      orders.insert(product_type.clone(), Mutex::new(BuySellQueue::default()));
    }

    Engine { orders }
  }

  /// Process new buy/sell order by finding matching pair.
  /// Items are processed in FIFO order, and maching record is removed.
  /// If no match found, then order is stored for later usage.
  pub fn process(&self, order: Order) -> Result<TradeResult, Error> {
    // Lock registry for given product type and get both buy/sell queues.
    // For buy orders 'sell queue' is considered as opposite.
    let mut product_orders = self
      .orders
      .get(order.product_box().product_type())
      .ok_or(Error::StorageNotFound)?
      .lock()?;
    let (queue, opposite_queue) = product_orders.get_queues(order.order_type());

    // Try to find value that would make buy-sell pair.
    // Matching order is popped from queue and TradeResult is returned.
    if let Some(opposite_client) = opposite_queue.pop_front() {
      return Ok(TradeResult::Transaction(
        *order.client_id(),
        opposite_client,
      ));
    }

    // If there is no matching pair then simply store order.
    queue.push_back(*order.client_id());

    Ok(TradeResult::NoMatch)
  }
}
