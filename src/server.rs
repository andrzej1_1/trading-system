use std::sync::Arc;

use thiserror::Error as ThisError;
use tokio::net::TcpListener;
use tokio::sync::broadcast;

use trading_system::core::engine::Engine;

use crate::client_handler::ClientHandler;
use crate::logger;

/// Server error.
#[derive(ThisError, Debug)]
pub enum Error {
  /// Problem with binding.
  #[error("Unable to bind.")]
  Binding,
  /// Unable to create handler.
  #[error("Handler failure.")]
  Handler,
}

/// Server for handling trade orders.
pub struct Server {
  /// Trading engine.
  engine: Arc<Engine>,
  /// Broadcast channel.
  broadcast_tx: broadcast::Sender<String>,
}

impl Server {
  /// Constructs new server.
  pub fn new() -> Self {
    let engine = Arc::new(Engine::new());
    // Channel with capacity 1 is enough, as we do *not* care about slow receivers.
    let (broadcast_tx, _) = broadcast::channel(1);
    Self {
      engine,
      broadcast_tx,
    }
  }

  /// Spawns TCP server and accept client connecions.
  /// Connections are then handled using ClientHandler.
  pub async fn run(&self, address: &str) -> Result<(), Error> {
    // Bind TCP server.
    let listener = TcpListener::bind(address)
      .await
      .map_err(|_e| Error::Binding)?;
    logger::log(format!("Server is listening on {}", address));

    // Accept connections and process them.
    loop {
      match listener.accept().await {
        Ok((stream, _address)) => {
          // Create handler with reference to the engine and the broadcast channels.
          let mut handler = ClientHandler::new(
            stream,
            self.engine.clone(),
            self.broadcast_tx.clone(),
            self.broadcast_tx.subscribe(),
          )
          .map_err(|_e| Error::Handler)?;

          // Run client handler.
          tokio::spawn(async move {
            handler.run().await.unwrap_or_else(|err| {
              eprintln!("Error occured during handling connection: {}", err);
            });
          });
        }
        Err(e) => {
          eprintln!("Client connection failed: {}", e);
        }
      }
    }
  }
}
